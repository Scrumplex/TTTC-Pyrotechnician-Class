CLASS.AddClass("PYROTECHNICIAN", {
    color = Color(255, 127, 0, 255),
    passiveWeapons = {
        "fp", -- ttt fassinator
        "weapon_impact" -- ttt impact pistol
    },
    lang = {
        name = {
            English = "Pyrotechnician",
            Deutsch = "Pyrotechniker"
        },
        desc = {
            English = "If it looks weird, blow it up!",
            Deutsch = "Wenn's krumm aussieht, jag's hoch!"
        }
    }
})
